
/* KD tree experiment */

#include "rplyx.h"
// #include "rply.h"
// #include "ply_mesh.h"
// #include <iostream>
// #include <stdio.h>
// #include <vector>
// #include <stdint.h>
// #include <stdlib.h>

const unsigned int MAX_CHUNK_SIZE = 64;

using namespace std;

// typedef uint32_t KdNodeindex;
// typedef uint32_t MboxIndex;


//##############MOVED PLY MESH TO SEPARATE HEADER FILE###########
// class PlyMesh {
// public:
//   PlyMesh() :
//     nrVerts(0), nrFaces(0), findex(0), fsize(0) {}
//   unsigned int nrVerts, nrFaces, findex;
//   vector<float> verts;
//   vector<VertIndex> faces;
//   unsigned int face[4]; /* buffer for reading quad indices */
//   short fsize; 
//   };


int read_cb(p_ply_argument arg)
  {
  unsigned int index, offset;
  double val;
  PlyMesh *m;

  ply_get_argument_user_data(arg, (void**)&m, (long*)&offset);
  val = ply_get_argument_value(arg);
  m->verts.push_back((float)val);
  if (offset == 2) m->nrVerts++;
  return 1;
  }

int face_cb(p_ply_argument arg)
  {
  long index, val;
  PlyMesh *m;

  ply_get_argument_user_data(arg, (void**)&m, NULL);
  ply_get_argument_property(arg, NULL, NULL, &index);
  val = ply_get_argument_value(arg);

  if (index < 0)
    m->fsize = (int) val;
  else
    {
    m->face[index] = (unsigned int)val;
    if (index == m->fsize-1)
      {
      m->faces.push_back(m->face[0]);
      m->faces.push_back(m->face[1]);
      m->faces.push_back(m->face[2]);
      m->nrFaces++;
      if (m->fsize == 4)
        {
        m->faces.push_back(m->face[3]);
        m->faces.push_back(m->face[0]);
        m->faces.push_back(m->face[2]);
        m->nrFaces++;
        }
      }
    }
  return 1;
  }

PlyMesh load_ply(char *filename){
  p_ply handle;
  p_ply_element elem;
  const char *name;
  long ninst;

  PlyMesh mesh;  

  if (!(handle = ply_open(filename, NULL, 0, NULL)))
    {
    cout << "Cannot open file.\n" << endl;
    exit(1);
    }

  if (!(ply_read_header(handle)))
    {
    cout << "Cannot read header.\n" << endl;
    exit(1);
    }

  long nrvs = ply_set_read_cb(handle, "vertex", "x", read_cb, &mesh, 0);
  ply_set_read_cb(handle, "vertex", "y", read_cb, &mesh, 1);
  ply_set_read_cb(handle, "vertex", "z", read_cb, &mesh, 2);
  long nrfs = ply_set_read_cb(handle, "face", "vertex_index", face_cb, &mesh, 0);

  cout << "File stats: " << nrvs << " verts, " << nrfs << " faces\n";
  if (!ply_read(handle))
    {
    cout << "Unable to read/parse ply file." << endl;
    exit(1);
    }

  ply_close(handle);

  return mesh;
}

void print_ply(PlyMesh mesh){
  cout << "Vertices\n";
  for (int v = 0, i = 0; v < mesh.nrVerts; v++, i +=3 )
    cout << mesh.verts[i] << "\t" << mesh.verts[i+1] << "\t" << mesh.verts[i+2] << endl;

  cout << "\nFaces\n";
  for (int f = 0, i = 0; f < mesh.nrFaces; f++, i +=3 )
    cout << mesh.faces[i] << "\t" << mesh.faces[i+1] << "\t" << mesh.faces[i+2] << endl;
}

int main(int argc, char **argv)
{
  if (argc < 2)
    {
    printf("Missing filename.\n");
    exit (1);
    }
  
  PlyMesh mesh = load_ply(argv[1]);

  print_ply(mesh);
  
  return 0;
}
