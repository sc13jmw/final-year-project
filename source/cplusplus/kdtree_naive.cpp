#include "rplyx.h"

#ifndef INF
#define INF INFINITY
#endif

//type alias for primitive
//store integer on primitive
//for example a primitives' index in an array or vector 
typedef int Primitive; 

class Vertex{
public:
	// Vertex Methods
	Vertex(float _x=0, float _y=0, float _z=0)
		: x(_x), y(_y), z(_z) {}
	float operator[](int i) const { return (&x)[i]; }//c++ store xyz in memory order
	float &operator[](int i) { return (&x)[i]; }
	// Vertex Public Data
	float x,y,z;
};

struct KdNode {
	// KdNode Methods

	~KdNode(){
		free(primitives);
	}
	/*
	** @arguments: array of primitive indices; number of primitives
	**				overlapping leaf region;
	*/
	void initLeaf(int *primNums, int np, Primitive *prims) {
		nPrims = np << 2;//number of primitives needs to be more accurate so shift to left
		flags |= 3;
		// Store _Primitive *_s for leaf node
		if (np == 0)
			onePrimitive = NULL;
		else if (np == 1)
			onePrimitive = &prims[primNums[0]];
		else {
			primitives = (Primitive **) malloc(np * sizeof(Primitive *));
			for (int i = 0; i < np; ++i)
				primitives[i] = &prims[primNums[i]];
		}
	}
	void initInterior(int axis, float s) {
		split = s;	//store split value
		flags &= ~3; //resets 3 most significant with (flags AND (NOT 11)) == (flags AND 00)
		flags |= axis; //means flags = flags OR axis
	}
	float SplitPos() const { return split; }
	int nPrimitives() const { return nPrims >> 2; }//
	int SplitAxis() const { return flags & 3; } //AND operation to test which axis are flags 
	bool IsLeaf() const { return (flags & 3) == 3; } //if node is leaf 3 most significant bits are 111

	//allows one portion or memory to be accessed by different types (www.cplusplus.com, other data types)
	//so different data types can share memory so only one variable needs to be stored
	//however care must be taken to ensure variables are read correctly
	//bit operations can also be used to store several variables within the same
	//block of memory at the cost of the most significant bits lowering accuracy
	union {
		unsigned int flags;   // Both - held in the 2 lest significant bits (RHS)
		float split;   // Interior
		unsigned int nPrims;  // Leaf
	};
	union {
		unsigned int rightChild;           // Interior
		Primitive *onePrimitive;  // Leaf pointer to one primitive
		Primitive **primitives;   // Leaf array of pointers to primitives
	};
};

class BBox{
public:	
	BBox() {
		pMin = Vertex( INF,  INF,  INF);
		pMax = Vertex(-INF, -INF, -INF);
	}
	BBox(const Vertex &p) : pMin(p), pMax(p) { }
	BBox(const Vertex &p1, const Vertex &p2) {
		pMin = Vertex(min(p1.x, p2.x),
					 min(p1.y, p2.y),
					 min(p1.z, p2.z));
		pMax = Vertex(max(p1.x, p2.x),
					 max(p1.y, p2.y),
					 max(p1.z, p2.z));
	}

	Vertex pMin, pMax;
};

struct BoundEdge {
	// BoundEdge Public Methods
	BoundEdge() { }
	BoundEdge(float tt, int pn, bool starting) {
		t = tt;
		primNum = pn;
		type = starting ? START : END;
	}
	bool operator<(const BoundEdge &e) const {
		if (t == e.t)
			return (int)type < (int)e.type;
		else return t < e.t;
	}
	float t;
	int primNum;
	enum { START, END } type;
};

class  KdTree {
	public:
		KdTree::
			KdTree(vector<Primitive> &p,
			int icost, int scost,
			float ebonus, int maxp, int maxDepth);
		BBox WorldBound() const { return bounds; }
//		bool CanIntersect() const { return true; }
		~KdTree();
		void buildTree(int nodeNum, const BBox &bounds,
		    const vector<BBox> &primBounds,
			int *primNums, int nprims, int depth,
			BoundEdge *edges[3], int *primsLeft, int *primsRight);
//		Primitive[] void searchTree();

	private:
		// KdTreeAccel Private Data
		int isectCost, traversalCost, maxPrims;
		float emptyBonus;
		unsigned int nPrims;
		Primitive *primitives;
		mutable int curPrimitiveId;
		KdNode *nodes;
		int nAllocedNodes;  //record the total number of nodes that has been allocated
		int nextFreeNode; //record the next node in this array that is available
		BBox bounds;
};
struct KdToDo {
	const KdNode *node;
	float tmin, tmax;
};

// KdTree Method Definitions
KdTree::
    KdTree(vector<*Primitive> &p,
		int icost, int tcost,
		float ebonus, int maxp, int maxDepth)
	: isectCost(icost), traversalCost(tcost),
	maxPrims(maxp), emptyBonus(ebonus) {

	vector<*Primitive> prims;
	/*not needed as Primitive is only int
	** for (u_int i = 0; i < p.size(); ++i)
	** 	p[i]->FullyRefine(prims);*/
	
	// Initialize mailboxes for _KdTreeAccel_
	// curMailboxId = 0;
	nPrims = prims.size();
	primitives = (Primitive *)memalign(L1_CACHE_LINE_SIZE, nPrims *
		sizeof(Primitive));
	for (u_int i = 0; i < nPrims; ++i)
		new (&primitives[i]) Primitive(prims[i]);
	// Build kd-tree for accelerator
	nextFreeNode = nAllocedNodes = 0;
	if (maxDepth <= 0)
		// maxDepth =
		    // Round2Int(8 + 1.3f * Log2Int(float(prims.size()))); //change maxDepth to 30 for simplicity
		maxDepth = 30;
	// Compute bounds for kd-tree construction
	vector<BBox> primBounds;
	primBounds.reserve(prims.size());
	for (u_int i = 0; i < prims.size(); ++i) {
		BBox b = prims[i]->WorldBound();
		bounds = Union(bounds, b);
		primBounds.push_back(b);
	}
	// Allocate working memory for kd-tree construction
	BoundEdge *edges[3]; //three edges one for each axis
	for (int i = 0; i < 3; ++i)
		edges[i] = new BoundEdge[2*prims.size()];
	int *primsLeft = new int[prims.size()];
	int *primsRight = new int[(maxDepth+1) * prims.size()];
	// Initialize _primNums_ for kd-tree construction
	int *primNums = new int[prims.size()];
	for (u_int i = 0; i < prims.size(); ++i)
		primNums[i] = i;
	// Start recursive construction of kd-tree
	buildTree(0, bounds, primBounds, primNums,
	          prims.size(), maxDepth, edges,
			  primsLeft, primsRight);
	// Free working memory for kd-tree construction
	delete[] primNums;
	for (int i = 0; i < 3; ++i)
		delete[] edges[i];
	delete[] primsLeft;
	delete[] primsRight;
}
KdTreeAccel::~KdTreeAccel() {
	for (u_int i = 0; i < nPrims; ++i)
		primitives[i].~Primitive();
	FreeAligned(primitives);
	FreeAligned(nodes);
}

void KdTree::buildTree(){
// Get next free node from _nodes_ array
	if (nextFreeNode == nAllocedNodes) {
		int nAlloc = max(2 * nAllocedNodes, 512);//for small no allocated nodes size of tree is 512*size(kdnode)
		KdAccelNode *n = (KdAccelNode *)AllocAligned(nAlloc *
			sizeof(KdAccelNode));
		if (nAllocedNodes > 0) {
			memcpy(n, nodes,
			       nAllocedNodes * sizeof(KdNode));
			FreeAligned(nodes);
		}
		nodes = n;
		nAllocedNodes = nAlloc;
	}
	++nextFreeNode;
	// Initialize leaf node if termination criteria met
	if (nPrims <= maxPrims || depth == 0) {
		nodes[nodeNum].initLeaf(primNums, nPrims,
		                       primitives, arena);
		return;
	}
	// Initialize interior node and continue recursion
	// Choose split axis position for interior node
	int axis = -1, bestOffset = -1;
	float bestCost = INF;//initialised as very large number
	float oldCost = isectCost * float(nPrims);//initial cost of creating a leaf node
	Vector d = nodeBounds.pMax - nodeBounds.pMin;
	float totalSA = (2.f * (d.x*d.y + d.x*d.z + d.y*d.z));
	float invTotalSA = 1.f / totalSA;
	// Choose which axis to split along
	int axis;
	//which axis is the largest
	if (d.x > d.y && d.x > d.z) axis = 0;
	else axis = (d.y > d.z) ? 1 : 2;
	int retries = 0;
	retrySplit:

	// Initialize edges for _axis_
	for (int i = 0; i < nPrims; ++i) {
		int pn = primNums[i];
		const BBox &bbox = allPrimBounds[pn];
		edges[axis][2*i] =
		    BoundEdge(bbox.pMin[axis], pn, true);
		edges[axis][2*i+1] =
			BoundEdge(bbox.pMax[axis], pn, false);
	}
	sort(&edges[axis][0], &edges[axis][2*nPrims]);

	float pAve = (nodeBounds.pMin[axis] + nodeBounds.pMax[axis])*0.5; //set split as half bound

	int nLeft = 0, nRight = 0;
	for (int i = 0; i < 2*nPrims; ++i) {
		if ((edges[axis][i].type == BoundEdge::START) && (edge[axis][i].t < pAve)) {
			primsLeft[++nLeft] = edges[axis][i].primNum;
		}
		if ((edges[axis][i].type == BoundEdge::End) && (edge[axis][i].t > pAve)) {
			primsRight[++nRight] = edges[axis][i].primNum;
		}
	
	// Recursively initialize children nodes
	nodes[nodeNum].initInterior(axis, pAve)
	BBox boundsLeft = nodeBounds, boundsRight = nodeBounds;
	boundsLeft.pMax[axis] = boundsRight.pMin[axis] = pAve;
	buildTree(nodeNum+1, boundsLeft,
		allPrimBounds, primsLeft, nLeft, depth-1, edges,
		primsLeft, primsRight + nPrims);
	nodes[nodeNum].rightChild = nextFreeNode;
	buildTree(nodes[nodeNum].rightChild, boundsRight, allPrimBounds,
		primsRight, nRight, depth-1, edges,
		primsLeft, primsRight + nPrims);
}

// void KdTree::search(){
	
// }