/*Header for rplyx allows use of load and print ply methods 
* in other files.
* only require to import rplyx.h
*/


#ifndef RPLYX_H
#define RPLYX_H

#include "rply.h"
#include "ply_mesh.h"
#include <iostream>
#include <stdio.h>
#include <vector>
#include <stdint.h>
#include <stdlib.h>

typedef uint32_t KdNodeindex;
typedef uint32_t MboxIndex;

PlyMesh load_ply(char *filename);//load ply file into a ply mesh
void print_ply(PlyMesh mesh);//prints vertices and faces to console

#endif