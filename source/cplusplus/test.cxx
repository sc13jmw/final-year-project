#include "rplyx.h"

void test(){
	std::cout << "Anything";
}

int main(int argc, char **argv)
{
	if (argc < 2)
    {
    printf("Missing filename.\n");
    exit (1);
    } 
  
	PlyMesh mesh = load_ply(argv[1]);

	// std::cout << "TEST" << std::endl;
	print_ply(mesh);

	return 1;
}