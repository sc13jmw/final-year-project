/*	used to hold ply file data
*	can load ply file by using rplyx.h and load_ply(filename) method
*/	

#ifndef PLY_MESH_H
#define PLY_MESH_H


#include <vector>
#include <stdint.h>

using namespace std;

typedef uint32_t FaceIndex; // index of a face 
typedef uint32_t VertIndex; // index of a vertex

class PlyMesh {
public:
  PlyMesh() :
    nrVerts(0), nrFaces(0), findex(0), fsize(0) {}
  unsigned int nrVerts, nrFaces, findex;
  vector<float> verts;
  vector<VertIndex> faces;
  unsigned int face[4]; /* buffer for reading quad indices */
  short fsize;  /*used for face_cb */
  };

 #endif