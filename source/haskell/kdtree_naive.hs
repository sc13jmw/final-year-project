module Main where

import PLY
import PLY.Types
import qualified Data.ByteString.Char8 as B
import qualified Data.Vector.Storable as V
import qualified Data.Vector as D
import Linear
import System.Environment (getArgs)
import Control.Applicative
import System.IO (FilePath)
import Data.Number.Transfinite (infinity, negativeInfinity )

type Vertex = V3 Float
type Faces = D.Vector (D.Vector Scalar)
type Verts = V.Vector Vertex
type Axis = Int

data Mesh = Mesh { verts :: Verts, faces :: Faces }
  deriving Show

-- instance Show Mesh where
--   show (Mesh vs _) = "(" ++ show x ++ ", " ++ show y ++ ", " ++ show x ++ ")"
--                       where 
--                         (V3 x y z) = take vs
--                         -- (map ((V.!) vs) [0..len])
--                         -- len = V.length vs


data KDnode = KDinner { axis :: Axis, value :: Float, left :: KDnode, right :: KDnode }
            | KDleaf  { prims :: [Int] }


data BBox = BBox { lo :: Vertex, hi :: Vertex }
  deriving Show

emptyB :: BBox 
emptyB = BBox (V3 inf inf inf) (V3 ninf  ninf  ninf)
          where inf = infinity
                ninf = negativeInfinity

-- minv, maxv :: Mesh  -> Vertex -> Vertex
-- minv = liftA2 min
-- maxv = liftA2 max

-- grow :: BBox -> Vertex -> BBox
-- grow (BBox l h) p = BBox (minv p l) (maxv p h)
-- grow :: (Vertex, Vertex) -> Vertex -> (Vertex, Vertex)
-- grow (smin, smax) temp = ((minv smin temp), (maxv smax temp) )

bounds :: Mesh -> BBox
bounds m = BBox (minV vs len infV) (maxV vs len ninfV) where
              len = (V.length vs) - 1
              vs = (verts m)
              infV = (V3 inf inf inf) 
              ninfV = (V3 ninf  ninf  ninf)
              inf = infinity
              ninf = negativeInfinity

minV :: Verts -> Int -> Vertex -> Vertex
minV _ 0 vs = vs
minV vs i (V3 x1 y1 z1) = min bestV (minV vs (i-1) bestV)
  where
    (V3 x0 y0 z0) = vs V.! i
    bestV = (V3 x y z)
    x = min x0 x1
    y = min y0 y1
    z = min z0 z1

maxV :: Verts -> Int -> Vertex -> Vertex
maxV _ 0 vs = vs
maxV vs i (V3 x1 y1 z1) = max bestV (maxV vs (i-1) bestV)
  where
    (V3 x0 y0 z0) = vs V.! i
    bestV = (V3 x y z)
    x = max x0 x1
    y = max y0 y1
    z = max z0 z1

build :: Mesh -> KDnode
build = undefined

bestAxis :: BBox -> Axis
bestAxis (BBox (V3 lx ly lz) (V3 hx hy hz)) 
            | (xd > yd) && (xd > zd) = 0
            | yd > zd              = 1
            | otherwise            = 2
            where
              xd = abs(lx - hx)
              yd = abs(ly - hy)
              zd = abs(lz - hz)

showAxis :: Int -> Char
showAxis a 
    | a == 0 = 'x'
    | a == 1 = 'y'
    | a == 2 = 'z'
    | otherwise = error "Incorrect Axis"            

largeNodes :: Mesh -> [KDnode] -> ([KDnode], [KDnode], [KDnode])
largeNodes = undefined

load :: FilePath -> IO Mesh
load fn = do {header <- loadHeader fn;
             case header of
                Left err -> error "Unable to load ply header."
                Right pd -> return $ Mesh (loadVerts pd) (loadFaces pd)}

loadVerts :: PLYData -> Verts
loadVerts pd = case loadPlyElementsV3 (B.pack "vertex") pd of
                Left err -> error "Unable to load vertics."
                Right vs -> vs  

loadFaces :: PLYData -> Faces
loadFaces pd = case loadPlyElements (B.pack "face") pd of
                Left err -> error "Unable to load faces."
                Right fs -> fs

main :: IO ()
-- main = getArgs >>= load >> putStrLn "Test"
          
main = do { fnames <- getArgs
          ; m <- load (head fnames)
          ; let b = bounds m 
          ; let ax = bestAxis b
          ; let axV = showAxis ax
          ; let fn = D.length (faces m)
          ; let vn = V.length (verts m)
          -- ; putStrLn $ "No. of vertices: " ++ show vn
          -- ; putStrLn $ "No. of faces: " ++ show fn
          ; putStrLn $ "Bound: " ++ show b
          ; putStrLn $ "BestAxis: " ++ show axV
          }