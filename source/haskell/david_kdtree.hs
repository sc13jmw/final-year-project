
{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import PLY
import PLY.Types
import qualified Data.ByteString.Char8 as B
import qualified Data.Vector.Storable as V
import qualified Data.Vector as D
import Linear
import System.Environment (getArgs)
import Control.Applicative

type Vertex = V3 Float
type Faces = D.Vector (D.Vector Scalar)
type Verts = V.Vector Vertex


data Mesh = Mesh { verts :: Verts, faces :: Faces }

data KDnode = KDinner { axis :: Int, value :: Float, left :: KDnode, right :: KDnode }
            | KDleaf  { prims :: [Int] }


data BBox = BBox { lo :: Vertex, hi :: Vertex }

emptyB :: BBox 
emptyB = undefined

minv, maxv :: Vertex -> Vertex -> Vertex
minv = liftA2 min
maxv = liftA2 max

grow :: BBox -> Vertex -> BBox
grow (BBox l h) p = BBox (minv p l) (maxv p h)

bounds :: Mesh -> BBox
-- bounds m = D.map (D.foldl' grow emptyB) (faces m)
bounds = undefined


build :: Mesh -> KDnode
build = undefined

largeNodes :: Mesh -> [KDnode] -> ([KDnode], [KDnode], [KDnode])
largeNodes = undefined

load :: String -> IO Mesh
load fn = do header <- loadHeader fn
             case header of
               Left err -> error "Unable to load ply header."
               Right pd -> do let vs :: V.Vector (V3 Float) = case loadPlyElementsV3 (B.pack "vertex") pd of
                                         Left err -> error "Unable to load vertics."
                                         Right vs -> vs
                              let fs = case loadPlyElements (B.pack "face") pd of
                                         Left err -> error "Unable to load faces."
                                         Right fs -> fs
                              return $ Mesh vs fs

main = do [fname] <- getArgs
          m <- load fname
          putStrLn "Loaded file"