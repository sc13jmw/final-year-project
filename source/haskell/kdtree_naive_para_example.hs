module Main where

import PLY
import PLY.Types
import qualified Data.ByteString.Char8 as B
import qualified Data.Vector.Storable as V
import qualified Data.Vector as D
import Linear
import System.Environment (getArgs)
import Control.Applicative
import System.IO (FilePath)

type Vertex = V3 Float
type Faces = D.Vector (D.Vector Scalar)
type Verts = V.Vector Vertex


data Mesh = Mesh { verts :: Verts, faces :: Faces }
  deriving Show


-- instance Show Mesh where
--   show (Mesh vs _) = putStrLn "(" ++ show x ++ ", " ++ show y ++ ", " ++ show x ++ ")"
--                       where (V3 x y z) = take vs

data KDnode = KDinner { axis :: Int, value :: Float, left :: KDnode, right :: KDnode }
            | KDleaf  { prims :: [Int] }


data BBox = BBox { lo :: Vertex, hi :: Vertex }
  deriving Show

emptyB :: BBox 
emptyB = undefined

-- minv, maxv :: Vertex -> Vertex -> Vertex
-- minv = liftA2 min
-- maxv = liftA2 max

-- grow :: BBox -> Vertex -> BBox
-- grow (BBox l h) p = BBox (minv p l) (maxv p h)
-- grow :: (Vertex, Vertex) -> Vertex -> (Vertex, Vertex)
-- grow (smin, smax) temp = ((minv smin temp), (maxv smax temp) )

import Control.Parallel (par, pseq)

bounds :: Mesh -> BBox
bounds m = BBox (par minVert) (pseq maxVert) where
              vs = (verts m)
              minI = V.minIndex vs
              maxI = V.maxIndex vs
              minVert = vs V.! minI
              maxVert = vs V.! maxI

build :: Mesh -> KDnode
build = undefined

largeNodes :: Mesh -> [KDnode] -> ([KDnode], [KDnode], [KDnode])
largeNodes = undefined

load :: FilePath -> IO Mesh
load fn = do {header <- loadHeader fn;
             case header of
                Left err -> error "Unable to load ply header."
                Right pd -> return $ Mesh (loadVerts pd) (loadFaces pd)}

loadVerts :: PLYData -> Verts
loadVerts pd = case loadPlyElementsV3 (B.pack "vertex") pd of
                Left err -> error "Unable to load vertics."
                Right vs -> vs  

loadFaces :: PLYData -> Faces
loadFaces pd = case loadPlyElements (B.pack "face") pd of
                Left err -> error "Unable to load faces."
                Right fs -> fs

main :: IO ()
-- main = getArgs >>= load >> putStrLn "Test"
          
main = do { fnames <- getArgs
          ; m <- load (head fnames)
          ; let b = bounds m 
          ; putStrLn $ "Bound: " ++ show b
          }