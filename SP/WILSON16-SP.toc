\contentsline {section}{\numberline {1}Background Research}{2}
\contentsline {subsection}{\numberline {1.1}Context}{2}
\contentsline {subsection}{\numberline {1.2}Problem Statement}{2}
\contentsline {subsection}{\numberline {1.3}Possible Solution}{2}
\contentsline {subsection}{\numberline {1.4}Evaluation Criteria}{3}
\contentsline {section}{\numberline {2}Project Scope}{3}
\contentsline {subsection}{\numberline {2.1}Aim}{3}
\contentsline {subsection}{\numberline {2.2}Objectives}{3}
\contentsline {subsection}{\numberline {2.3}Deliverables}{3}
\contentsline {section}{\numberline {3}Project Schedule}{3}
\contentsline {subsection}{\numberline {3.1}Methodology}{3}
\contentsline {subsection}{\numberline {3.2}Task, Milestones and Time-line}{4}
\contentsline {section}{Appendices}{5}
\contentsline {section}{\numberline {A}Appendix A}{5}
