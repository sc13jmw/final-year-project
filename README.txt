This version control repository contains all work for my final year project. 
This includes: 
SP - All files and reports related to the scoping and planning document submitted in Week 3
logs - typed notes for some meeting with supervisor, David Duke
report/fyp - All LaTex source files and images used to produce the final porject report
source/cplusplus - All C++ source code files including a commented version of the PBRT KdtreeAccel (kdtree_pbrt.cpp), my implementation of this code (kdtree_naive.cpp), other source files required to compile NOT developed by me (rply.c, rply.h, rplyfile.h, ply_mesh.h, rplyx.cxx)

The C++ and Haskell source code files can be found at source/cplusplus and source/haskell respectfully. Within the cplusplus directory the is a makefile to compile the code and the Haskell can be compiles using the command 'ghc kdtree_naive.hs' though some dependencies may be required to do this. Both the C++ and Haskell require a ply file to run, some can be found at source/ply. 